/**
 * Created by DOrdynskiy on 24.02.2016.
 */

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GUI extends Application {
    private Servo srv = null;
    // Окно приложения
    private Stage window;
    private boolean closed = false;
    public static String logText = "EchoWebServer stopped";
    private static int logSize = 0;
    public static String titleText = "Test EchoWebServer v1.7 - http://murrzik.su/servo/";
    public static String enc = "utf-8";
    private static String startedPort = "";

    // Инициализация приложения
    public void init(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Описание поля для ввода порта
        Label portLabel = new Label("Insert server port - ");
        // Создание текстового поля для ввода порта
        TextField portField = new TextField("8080");

        // Кнопка запуска сервера
        Button servoStartButton = new Button("Start listening");
        // Минимальный размер кнопки
        servoStartButton.setMinWidth(200);

        // Кнопка остановки сервера
        Button servoStopButton = new Button("Stop listening");
        // Минимальный размер кнопки
        servoStopButton.setMinWidth(200);

        // Событие по нажатию на кнопку остановки сервера
        servoStopButton.setOnAction(e -> {
            servoStop();
        });


        // Горизонтальный контейнер, для поля ввода порта
        HBox ititFields = new HBox(4);
        // Добавление поля ввода в контейнер
        ititFields.getChildren().addAll(portLabel, portField, servoStartButton, servoStopButton);
        // Выравнивание контейнера по центру окна
        ititFields.setAlignment(Pos.CENTER);

        // Вертикальный контейнер для всех компонентов
        VBox layout = new VBox(4);
        // Выравнивание контейнера по центру окна
        layout.setAlignment(Pos.CENTER);
        // Добавление в контейнер контейнера с полями ввода и кнопки


        Label reqLabel = new Label("  Input requests:");

        TextArea logTxt = new TextArea();
        logTxt.setPrefRowCount(19);
        logTxt.setPrefWidth(2500);
        //logTxt.setMaxWidth(2500);
        logTxt.setPrefColumnCount(100);
        logTxt.setWrapText(true);

        // Кнопка очистки логов сервера
        Button clearLogsButton = new Button("Clear logs");
        // Минимальный размер кнопки
        clearLogsButton.setMinWidth(200);

        // Событие по нажатию на кнопку очистки логов сервера
        clearLogsButton.setOnAction(e -> {
            logText = (srv != null && srv.isAlive()) ? "Server started at port " + startedPort : "EchoWebServer stopped";
        });

        VBox reqBox = new VBox(3);
        reqBox.setAlignment(Pos.CENTER_LEFT);
        reqBox.getChildren().addAll(reqLabel, logTxt, clearLogsButton);

        Label respLabel = new Label("  Output response body:");

        TextArea respTxt = new TextArea();
        respTxt.setPrefRowCount(19);
        respTxt.setMinWidth(250);
        respTxt.setMaxWidth(250);
        respTxt.setPrefColumnCount(100);
        respTxt.setWrapText(true);
        respTxt.setText(Servo.response);

        respTxt.textProperty().addListener((observable, oldValue, newValue) -> {
            Servo.response = newValue;
        });

        ObservableList<String> respOoptions =
                FXCollections.observableArrayList(
                        "200 OK",
                        "404 Not Found"
                );
        ComboBox respComboBox = new ComboBox(respOoptions);
        respComboBox.setEditable(false);
        respComboBox.setValue("200 OK");
        respComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                if (t1.equals("200 OK")) respTxt.setText("HTTP/1.1 200 OK\nContent-Type: text/xml\nContent-Encoding: gzip\nServer: Jetty(6.1.26)");
                else if (t1.equals("404 Not Found")) respTxt.setText("HTTP/1.1 404 Not Found\nContent-Type: text/xml\nContent-Encoding: gzip\nServer: Jetty(6.1.26)");
            }
        });

        Label timeoutLabel = new Label("Timeout ms: ");
        TextField timeoutTextField = new TextField("0");
        timeoutTextField.setMaxWidth(50);

        timeoutTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            Servo.responseTimeout = Integer.parseInt(newValue);
        });

        HBox respControls = new HBox(3);
        respControls.setAlignment(Pos.CENTER);
        respControls.getChildren().addAll(respComboBox, timeoutLabel, timeoutTextField);

        VBox respBox = new VBox(3);
        respBox.setAlignment(Pos.CENTER_LEFT);
        respBox.getChildren().addAll(respLabel, respTxt, respControls);

        // Событие по нажатию на кнопку запуска сервера
        servoStartButton.setOnAction(e -> {
            Servo.response = respTxt.getText();
            servoStartThread(portField);
        });
        // Событие по нажатию "Enter" в поле ввода порта
        portField.setOnAction(e -> {
            Servo.response = respTxt.getText();
            servoStartThread(portField);
        });

        HBox tAreas = new HBox(2);
        // Добавление кнопок в контейнер
        tAreas.getChildren().addAll(reqBox, respBox);
        tAreas.setAlignment(Pos.CENTER);

        CheckBox autoUbdScrl = new CheckBox("Autoupdate  |  ");
        autoUbdScrl.setSelected(true);

        CheckBox autoScrlCB = new CheckBox("Autoscroll");
        autoScrlCB.setSelected(true);

        Label encLabel = new Label("  |  Select encoding:");

        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "utf-8",
                        "cp1251",
                        "cp1252",
                        "US-ASCII"
                );
        ComboBox encComboBox = new ComboBox(options);
        encComboBox.setEditable(true);
        encComboBox.setValue("utf-8");
        encComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                if (null != t1 && t1.length() > 0 && t1.length() < 15) enc = t1.toLowerCase();
            }
        });

        HBox cb1 = new HBox(4);
        // Добавление кнопок в контейнер
        cb1.getChildren().addAll(autoUbdScrl, autoScrlCB, encLabel, encComboBox);
        cb1.setAlignment(Pos.CENTER);

        layout.getChildren().addAll(ititFields, tAreas, cb1);

        Scene scene = new Scene(layout, 700, 450);

        /*scene.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) -> {
            System.out.println("Width: " + newSceneWidth);
        });*/

        scene.heightProperty().addListener((observableValue, oldSceneHeight, newSceneHeight) -> {
            if(newSceneHeight.doubleValue() > 411.0) {
                logTxt.setPrefHeight(newSceneHeight.doubleValue() - 108.0);
                respTxt.setPrefHeight(newSceneHeight.doubleValue() - 108.0);
            }
        });

        new java.lang.Thread(() -> {
            while (!closed) {
                if (autoUbdScrl.isSelected() && logText.length() != logSize) {
                    logSize = logText.length();
                    logTxt.clear();
                    logTxt.setText(logText);
                    if (autoScrlCB.isSelected()) logTxt.setScrollTop(Double.MAX_VALUE);
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    logText = e.getMessage();
                }
            }
        }).start();
        // Применение стиля CSS
        scene.getStylesheets().add("css/Evil.css");

        // Инициализация окна приложения
        window = primaryStage;
        // Обработка события - закрытие окна приложения
        window.setOnCloseRequest(event -> {
            servoStop();
            closed = true;
            Platform.exit();
            System.exit(0);
        });
        // Установка заголовка окна
        window.setTitle(titleText);
        // Добавление панели с компонентами на окно приложения
        window.setScene(scene);
        // Установка размеров окна приложения
        window.setWidth(700);
        window.setHeight(450);
        // Отображение окна приложения
        window.show();
    }

    private void servoStartThread(final TextField portField) {
        // Формирование анонимного класса - потока создания сервера
        Task<Void> task = new Task<Void>() {
            @Override
            // Создание метода, запускающегося при старте потока
            protected Void call() throws Exception {
                servoStart(portField.getText());
                return null;
            }
        };
        // Создание нового потока
        Thread t = new Thread(task);
        // Запуск потока создания сервера
        t.start();
    }

    private void servoStart(String servoPort) {
        // Если порт состоит из более чем 5 цыфр, то не запускать сервер
        if (servoPort == null || servoPort.length() > 5 || servoPort.isEmpty() || servoPort.replaceAll("\\d*", "").length() != 0 || Integer.parseInt(servoPort) > 65535) return;
        // Если сервер уже запущен, не запускать новый
        if (srv != null && srv.isAlive()) return;

        try {
            if (srv == null) srv = new Servo();
            Servo.killServo = false;
            srv.start(servoPort);
            startedPort = servoPort;
        } catch (Throwable throwable) {
            logText = throwable.getMessage();
        }
    }

    private void servoStop() {
        // Если сервер работает, остановить его
        if (srv != null && srv.isAlive()) {
            Servo.killServo = true;
            srv.stop();
        }
    }
}
