import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by DOrdynskiy on 01.03.2016.
 */
public class Servo {
    private static GUI window;
    private Thread t = null;
    public static boolean killServo = true;
    public static Socket s;
    public static InputStream is;
    public static OutputStream os;
    public static ServerSocket ss;
    public static String response = "HTTP/1.1 200 OK\n" +
            "Content-Type: text/xml\n" +
            "Content-Encoding: gzip\n" +
            "Server: Jetty(6.1.26)";
    public static int responseTimeout = 0;

    public static void main(String[] args) {
        // Создание объекта окна авторизации
        window = new GUI();
        window.init(args);
    }

    public void start(String aPort) {
        try {
            t = new Thread(new SocketProcessor(aPort));
            t.start();
        } catch (Throwable throwable) {
        }
    }

    private class SocketProcessor implements Runnable {
        private String port = "";

        private SocketProcessor(String aPort) throws Throwable {
            port = aPort;
        }

        public void run() {
            try {
                ss = new ServerSocket(Integer.parseInt(port));
            } catch (IOException e) {
                GUI.logText = e.getMessage();
                stop();
                return;
            }
            try {
                GUI.logText = "Server started at port " + port;
                while (!ss.isClosed()) {
                    s = ss.accept();
                    s.setKeepAlive(false);
                    is = s.getInputStream();
                    os = s.getOutputStream();

                    Map<String, String> headers = readInputHeaders(is);
                    try {
                        Thread.sleep(responseTimeout);
                    } catch (InterruptedException e) {
                        GUI.logText += "\n" + e.getMessage() + "\n";
                    }
                    os.write(response.getBytes());

                    is.close();
                    os.flush();
                    os.close();
                    s.close();

                    GUI.logText += "\n_______________________________________";
                    GUI.logText += "\nConnected client: " + s.getInetAddress();
                    GUI.logText += "\n" + new Date().toString();
                    GUI.logText += "\nThis server encoding = " + GUI.enc;
                    GUI.logText += "\n";
                    if(headers.keySet().contains("post_data")) GUI.logText += "post_data :" + headers.get("post_data");
                    for (String key : headers.keySet()) {
                        if(!key.equals("post_data")) GUI.logText += "\n" + key + " : " + headers.get(key);
                    }
                    GUI.logText += "\n_______________________________________";
                    GUI.logText += "\n";

                    if (killServo) break;
                }
            } catch (IOException e) {
                GUI.logText += "\n\n" + e.getMessage();
            }

        }

        private Map<String, String> readInputHeaders(InputStream is) throws IOException {
            Map<String, String> request = new HashMap<>();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuffer headersSB = new StringBuffer();
            boolean isPost = false;
            while (true) {
                String s = new String(br.readLine().getBytes(GUI.enc));
                if (s == null || s.isEmpty()) {
                    if (isPost) {
                        StringBuffer requestContent = new StringBuffer();
                        while(br.ready()) {
                            int u = br.read();
                            if(u < 0) break;
                            char c = (char) u;
                            requestContent.append(c);
                        }
                        request.put("post_data", "\n\n" + new String(requestContent.toString().getBytes(GUI.enc)).trim() + "\n\n");
                    }
                    break;
                }
                headersSB.append(s);
                String key = s.contains(":") && !s.toLowerCase().startsWith("get") && !s.toLowerCase().startsWith("post") ? s.substring(0, s.indexOf(":")) : "method";
                String val = s.contains(":") && !s.toLowerCase().startsWith("get") && !s.toLowerCase().startsWith("post") ? s.substring(s.indexOf(":") + 1).trim() : s.trim();
                request.put(key, val);
                if (s.toLowerCase().contains("content-length")) isPost = true;
            }

            return request;
        }
    }

    public void stop() {
        try {
            if(ss != null) ss.close();
        } catch (IOException e) {
            GUI.logText += "\n\n" + e.getMessage();
            return;
        }
        t.interrupt();
        GUI.logText += "\n\nEchoWebServer stopped";
        killServo = true;
    }

    public boolean isAlive() {
        return (t != null && !t.isInterrupted() && !killServo);
    }
}
